const terser = require('@rollup/plugin-terser');
const { nodeResolve } = require('@rollup/plugin-node-resolve');

const bundle = (config) => ({
  ...config,
  plugins: [...(config.plugins || []), nodeResolve()],
  input: 'src/index.js',
  external: [ 'circular-json', 'kafkajs', 'lodash.defaultsdeep', 'uuid', 'winston-transport', 'winston' ],
});

module.exports = [
  bundle({
    plugins: [
      terser(),
    ],
    output: [
      {
        file: `dist/index.js`,
        format: 'cjs',
        sourcemap: true,
      },
    ],
  })
];
