module.exports = {
  root: true,
  env: {
    es2021: true,
  },
  globals: {
    rootDir: 'readonly',
  },
  extends: [
    'airbnb-base',
    'plugin:eslint-comments/recommended',
    'plugin:eslint-comments/recommended',
    'prettier',
  ],
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
  },
  rules: {
    'import/prefer-default-export': 'off',
    'import/no-extraneous-dependencies': 'off',
    'no-underscore-dangle': 'off',
    'no-param-reassign': [
      'error',
      {
        props: true,
        ignorePropertyModificationsFor: [
          'acc', // for reduce accumulators
          'accumulator', // for reduce accumulators
          'item', // for migration exec functions
        ],
      },
    ],
  },
};
