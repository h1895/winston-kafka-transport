# Kafka transport for Winston v3

Flexible and simple kafka transport for winston v3

## Usage

### Install package

`yarn add @hinted-public/winston-kafka-transport`

or

`npm install @hinted-public/winston-kafka-transport`

### Using KafkaTransport for winston

```js
const winston = require('winston');
const KafkaTransport = require('@hinted-public/winston-kafka-transport');

winston.add(
  new KafkaTransport({
    level: 'info',
    format: format.combine(...traceFormats),
    meta: {},
    kafkaOptions: {
      brokers: ['localhost:9092'],
      clientId: 'winston-kafka-logger',
    },
    topic: kafkaTopic,
    name: 'WinstonLogs',
    formatter: JSON.stringify,
  }),
);
```

