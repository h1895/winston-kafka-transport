const { Kafka } = require('kafkajs');
const Transport = require('winston-transport');
const CircularJSON = require('circular-json');
const defaultsDeep = require('lodash.defaultsdeep');
const { v4: uuidv4 } = require('uuid');
const { Console } = require("console");

const consoleLogger = new Console({ stdout: process.stdout, stderr: process.stderr });

const { LOGS_KAFKA_LOG_DELIVERY_STATUS } = process.env;

const DEFAULTS = {
  topic: 'winston-kafka-logs',
  kafkaOptions: {
    brokers: ['localhost:9092'], // required!
    clientId: 'winston-kafka-logger',
    connectionTimeout: 10 * 1000,
    requestTimeout: 30 * 1000,
    retry: {
      maxRetryTime: 30000,
      initialRetryTime: 300,
      retries: 5,
      restartOnFailure: true,
    },
  },
  partition: 0,
};

module.exports = class KafkaTransport extends Transport {
  constructor(options) {
    super(options);
    this.options = defaultsDeep({}, options || {}, DEFAULTS);

    this.timestamp = () => Date.now();
    this.jsonformatter = options.jsonformatter || CircularJSON;

    this.connected = false;
    this.client = new Kafka(this.options.kafkaOptions);
    this.producer = this.client.producer();
  }

  async connect() {
    try {
      await this.producer.connect();

      const { CONNECT, REQUEST_TIMEOUT } = this.producer.events;

      this.producer.on(CONNECT, () => {
        consoleLogger.info(
          `Connected to Kafka brokers: ${this.options.kafkaOptions.brokers}`
        );
        this.connected = true;
      });

      this.producer.on(REQUEST_TIMEOUT, (err) => {
        throw new Error(err);
      });
    } catch (err) {
      consoleLogger.info(err);
    }
  }

  // eslint-disable-next-line class-methods-use-this
  logSendingState(res) {
    if (LOGS_KAFKA_LOG_DELIVERY_STATUS) {
      if (res[0].errorCode === 0) {
        consoleLogger.info('Log successfully sent to Kafka');
      } else {
        consoleLogger.info(
          `An error occurred while sending log to kafka\nError code: ${res[0].errorCode}`
        );
      }
    }
  }

  // eslint-disable-next-line consistent-return
  async _sendPayload(payload) {
    const { CONNECT } = this.producer.events;

    try {
      if (!this.connected) {
        await this.connect();
        return this.producer.on(CONNECT, async () => {
          const res = await this.producer
            .send(payload)
            .catch((err) => consoleLogger.info(err));
          this.logSendingState(res);
        });
      }

      const res = await this.producer.send(payload);
      this.logSendingState(res);
      return res;
    } catch (err) {
      consoleLogger.info(err);
    }
  }

  disconnect() {
    consoleLogger.info('Disconnected from kafka');
    this.connected = false;
    return this.producer.disconnect();
  }

  log(info, callback) {
    const messageBuffer = Buffer.from(this.options.formatter(info));
    const message = JSON.parse(messageBuffer.toString());

    try {
      const payload = {
        topic: this.options.topic,
        messages: [
          {
            key: uuidv4(),
            value:
              typeof message === 'object'
                ? this.jsonformatter.stringify(message)
                : message,
            partition: this.options.partition,
          },
        ],
      };

      this._sendPayload(payload, (error) => {
        if (error) {
          consoleLogger.info(error);
        }
      });
      return callback(null, true);
    } catch (error) {
      consoleLogger.info(error);
      return callback(error);
    }
  }
};
